#ifndef TEMPLATE_H
#define TEMPLATE_H

// =========================== Constants ========================

// ============================ Macros ==========================

// ========================= API Typedefs =======================

// ====================== API Global Variables ==================
// extern uint8_t tmpl_someApiVariable;

// ========================== API methods =======================
#if defined(__cplusplus)
extern "C" {
#endif
// void tmpl_myApiFunc(void);


#if defined(__cplusplus)
}
#endif

#endif /* TEMPLATE_H */
