#include "bitmaps.h"

btm_bitmap_t btm_whitespaceLogo = {
    .widthbytes = 3,
    .height = 22,
    .buffer = {
        0,   128, 0,
        0,   130, 0,
        32,  139, 0,
        34,  130, 48,
        34,  202, 72,
        42,  170, 120,
        62,  170, 64,
        20,  169, 63,
        0,   0,   0,
        29,  152, 136,
        33,  69,  84,
        61,  93,  24,
        5,   85,  80,
        249, 140, 142,
        1,   4,   0,
        1,   0,   0,
        0,   0,   0,
        0,   0,   0,
        0,   0,   0,
        0,   0,   0,
        0,   0,   0,
        0,   0,   0, }
};

